#!/usr/bin/env python3.11

# pylint: disable = invalid-name
# pylint: disable = missing-docstring

# ----------
# Collatz.py
# ----------

"""
return the cycle length for a given integer
"""
def cycle_length(n: int) -> int:
    assert n > 0
    c = 1
    while n > 1:
        if (n % 2) == 0:
            n = n // 2
            c += 1
        else:
            n = n + (n >> 1) + 1
            c += 2
    assert c > 0
    return c


# ------------
# collatz_eval
# ------------

#global cache
cl = [0] * 1000000

"""
takes in the cycle length cache and returns it
take in the cache, the value and return the value
     if the value is in the cache, return the  Value
     if its not, get the cycle length, add it to the cache, and return it
"""
def cache_collatz(val: int) -> int:
    # if its not in the cache calculate it and add it to the cache
    if val >= len(cl):
        return cycle_length(val)
    if cl[val] == 0:
        cl[val] = cycle_length(val)
    # regardless return the cycle length
    return cl[val]



# take in the tuple and return the tuple with the max cycle length
def collatz_eval(t: tuple[int, int]) -> tuple[int, int, int]:
    """
    :param t tuple of the range
    :return max cycle length of the range, inclusive
    """
    i, j = t
    assert i > 0
    assert j > 0
    beg = i
    end = j
    if j < i:
        beg = j
        end = i
    # v is the max cycle length between i and j
    # optimization is m = (j / 2) + 1, m > i, mcl(i, j) = mcl(m, j)
    start = beg
    mid = (end // 2) + 1
    if mid > beg:
        start = mid
    # simply
    mcl = 1
    for x in range(start, end + 1):
        c = cache_collatz(x)
        if c > mcl:
            mcl = c
    assert mcl > 0
    return i, j, mcl

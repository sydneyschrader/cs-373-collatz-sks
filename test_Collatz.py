#!/usr/bin/env python3.11

# pylint: disable = invalid-name
# pylint: disable = missing-docstring

# ---------------
# test_Collatz.py
# ---------------

# -------
# imports
# -------

import unittest  # main, TestCase

from Collatz import collatz_eval

# ------------
# Test_Collatz
# ------------


class Test_Collatz(unittest.TestCase):
    def test_collatz_0(self) -> None:
        self.assertEqual(collatz_eval((1, 10)), (1, 10, 20))

    def test_collatz_1(self) -> None:
        self.assertEqual(collatz_eval((100, 200)), (100, 200, 125))

    def test_collatz_2(self) -> None:
        self.assertEqual(collatz_eval((201, 210)), (201, 210, 89))

    def test_collatz_3(self) -> None:
        self.assertEqual(collatz_eval((300, 400)), (300, 400, 144))

    def test_collatz_4(self) -> None:
        self.assertEqual(collatz_eval((401, 500)), (401, 500, 142))

    def test_collatz_5(self) -> None:
        self.assertEqual(collatz_eval((501, 600)), (501, 600, 137))

    def test_collatz_6(self) -> None:
        self.assertEqual(collatz_eval((601, 700)), (601, 700, 145))

    def test_collatz_7(self) -> None:
        self.assertEqual(collatz_eval((701, 800)), (701, 800, 171))

    def test_collatz_8(self) -> None:
        self.assertEqual(collatz_eval((801, 900)), (801, 900, 179))

    def test_collatz_9(self) -> None:
        self.assertEqual(collatz_eval((901, 1000)), (901, 1000, 174))

    def test_collatz_10(self) -> None:
        self.assertEqual(collatz_eval((1101, 1200)), (1101, 1200, 182))

    def test_collatz_11(self) -> None:
        self.assertEqual(collatz_eval((1201, 1300)), (1201, 1300, 177))

    def test_collatz_12(self) -> None:
        self.assertEqual(collatz_eval((1301, 1400)), (1301, 1400, 177))

    def test_collatz_13(self) -> None:
        self.assertEqual(collatz_eval((1500, 2000)), (1500, 2000, 180))

    def test_collatz_14(self) -> None:
        self.assertEqual(collatz_eval((3000, 10000)), (3000, 10000, 262))

    def test_collatz_15(self) -> None:
        self.assertEqual(collatz_eval((15000, 20000)), (15000, 20000, 279))

    def test_collatz_16(self) -> None:
        self.assertEqual(collatz_eval((123456, 234567)), (123456, 234567, 443))

    def test_collatz_17(self) -> None:
        self.assertEqual(collatz_eval((14, 47)), (14, 47, 112))

    def test_collatz_18(self) -> None:
        self.assertEqual(collatz_eval((200000, 700000)), (200000, 700000, 509))

    def test_collatz_19(self) -> None:
        self.assertEqual(collatz_eval((121202, 131303)), (121202, 131303, 344))


# ----
# main
# ----

if __name__ == "__main__":  # pragma: no cover
    unittest.main()
